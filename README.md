# nuxt

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).



# nuxt安装教程

  1.先安装vue-cli   cnpm install vue-cli -g
  2.vue init nuxt/starter
  3.然后会询问是否在当前目录创建，选择y回车，项目名称、项目介绍、作者想写的就写，不想写的直接回车即可，最后安装成功以后，查看项目文件内是否已经存在了nuxt的项目文件
  4.nuxt自动创建的项目文件目录 cnpm install
  5.cnpm run dev

# nuxt中目录部署示意
|-- .nuxt                            // Nuxt自动生成，临时的用于编辑的文件，build
|-- assets                           // 用于组织未编译的静态资源入LESS、SASS 或 JavaScript
|-- components                       // 用于自己编写的Vue组件，比如滚动组件，日历组件，分页组件
|-- layouts                          // 布局目录，用于组织应用的布局组件，不可更改。   相当于vue的app.vue
|-- middleware                       // 用于存放中间件
|-- pages                            // 用于存放写的页面，我们主要的工作区域
|-- plugins                          // 用于存放JavaScript插件的地方
|-- static                           // 用于存放静态资源文件，比如图片
|-- store                            // 用于组织应用的Vuex 状态管理。
|-- .editorconfig                    // 开发工具格式配置
|-- .eslintrc.js                     // ESLint的配置文件，用于检查代码格式
|-- .gitignore                       // 配置git不上传的文件
|-- nuxt.config.json                 // 用于组织Nuxt.js应用的个性化配置，已覆盖默认配置
|-- package-lock.json                // npm自动生成，用于帮助package的统一性设置的，yarn也有相同的操作
|-- package-lock.json                // npm自动生成，用于帮助package的统一性设置的，yarn也有相同的操作
|-- package.json                     // npm包管理配置文件


# nuxt官方安装教程
npx create-nuxt-app <项目名称>
# 配置详解
? Project name nuxttest							//项目名称
? Project description My astounding Nuxt.js project		//项目描述
? Author name forever									//作者姓名
? Choose programming language JavaScript			//选择开发的语言 javascript or 			typescript
? Choose the package manager Yarn				//选择包管理的工具 npm or yarn
? Choose UI framework Element					//选择一个UI框架
? Choose custom server framework Express			//选择一种服务端框架
? Choose Nuxt.js modules (Press <space> to select, <a> to toggle all, <i> to invert selection)
? Choose linting tools (Press <space> to select, <a> to toggle all, <i> to invert selection) //检查代码是否规范
? Choose test framework None						//选择一个测试框架
? Choose rendering mode Universal (SSR) 			//选择一种渲染模式 SSR or SPA
? Choose development tools (Press <space> to select, <a> to toggle all, <i> to invert selection)


# nuxt使用sass
cnpm i node-sass sass-loader scss-loader --save-dev  
在项目的assets目录下新建一个css目录，然后在该目录下新建一个index.scss文件，用这个文件作为scss的入口文件
在 nuxt.conf.js中，添加要使用的 CSS 资源：{ src: '~/assets/css/index.scss', lang: 'scss' }


# nuxt 路由
  nuxt路由根据pages下的文件目录生成
  pages默认进入index.vue

 # nuxt子路由
  创建内嵌子路由，需要添加一个Vue文件，同时添加一个与该文件同名的目录用来存放子视图组件
  在父组件(.vue文件) 内增加 <nuxt-child/> 用于显示子视图内容。
    <p>
      <nuxtLink to="/shop/shop">
        goto /shop
      </nuxtLink>
    </p>
    <p>
      <nuxtLink to="/shop/mine">
        goto /mine
      </nuxtLink>
    </p>
    <div>-----------------------------</div>

    <div>
      <nuxt-child />
    </div>


# nuxt 引入第三方js 如rem.js
  将js放在plugin文件下；plugins 属性配置的所有插件会在 Nuxt.js 应用初始化之前被加载导入。
  在nuxt.config.js里面插入 ,注意ssr必须为false
  plugins: [
    {src: '~plugins/rem.js', ssr: false}
  ],

# nuxt 使用axios以及跨域
  cnpm install axios @nuxtjs/axios @nuxtjs/proxy
  在nuxt.config.js中配置：
    modules: [
      '@nuxtjs/axios'
    ],
    axios: {
      proxy: true,
      prefix: '/api/',
      credentials: true
    },
    proxy: {
      '/api/': {
        target: 'http://maohemao.cn/api/',
        pathRewrite: {
          '^/api/': '/',
          changeOrigin: true
        }
      }
    },

    在.vue文件中调用
    this.$axios({})


# nuxt轮播图  https://www.jianshu.com/p/b57a7ca2b5c0
  cnpm install -S swiper vue-awesome-swiper
  cnpm install swiper --save


# 页面flex布局
  .container {
    font-size: 0.16rem;
    height: 100vh;
    background: #f5f5f5;
    display: flex;
    flex-direction: column;
  }
  header {
    width: 100%;
    height: 0.5rem;
    text-align: center;
    line-height: 0.5rem;
    background: #fff;
    font-size: 0.14rem;
  }
  .contents {
    width: 100%;
    flex: 1;
    overflow: scroll;
  }
  .footer {
    width: 100%;
    height: 0.5rem;
    display: flex;
    border-top: 1px solid #ccc;
    li {
      width: 25%;
    }
  }

# nuxt定义路由激活样式
  router: {
    // linkActiveClass: 'active-link'  //定义激活时的类名
    linkExactActiveClass: 'exact-active-link'  //定义激活的类名
  },
  


# nuxt 路由传参
  # query传参   query 查询的意思   会将参数显示在地址栏
  :to="{path:'/bannerInfo?id='+item.id}"
  :to="{path:'/bannerInfo',query:{id:123}}"

  传参: 
    this.$router.push({
            path:'/xxx'
            query:{
              id:id
            }
          })
      
    接收参数:
    this.$route.query.id

  # params传参   params 参数的意思  id会显示地址栏，其他参数不会显示在地址栏
  :to="{name:'personalTailor',params:{id:123}}"
  传参: 
    this.$router.push({
            name:'xxx'
            params:{
              id:id
            }
    })
      
    接收参数:
    this.$route.params.id

  获取参数：this.$route.params
  * 注意:params传参，push里面只能是 name:'xxxx',不能是path:'/xxx',因为params只能用name来引入路由，如果这里写成了path，接收参数页面会是undefined！！！
  * 另外，二者还有点区别，直白的来说query相当于get请求，页面跳转的时候，可以在地址栏看到请求参数，而params相当于post请求，参数不会再地址栏中显示
  * 注意:传参是this.$router,接收参数是this.$route


